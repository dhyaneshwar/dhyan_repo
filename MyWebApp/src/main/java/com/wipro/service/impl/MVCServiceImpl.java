package com.wipro.service.impl;

import org.springframework.stereotype.Service;

import com.wipro.service.interfaces.MVCService;

@Service("mvcService")
public class MVCServiceImpl implements MVCService {

	@Override
	public int add(int arg1, int arg2) {
		int result = 0;
		try {
			result = arg1 + arg2;
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}
