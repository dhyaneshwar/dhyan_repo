package com.wipro.service.impl;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class DisplayNameBeanPostProcessor implements BeanPostProcessor{

	@Override
	public Object postProcessBeforeInitialization(Object beanObj,String beanName) throws BeansException {
		System.out.println("Inside postProcessBeforeInitialization");
		System.out.println("Bean Name : " + beanName);
		return beanObj;
	}
	
	@Override
	public Object postProcessAfterInitialization(Object beanObj,String beanName) throws BeansException {
		System.out.println("Inside postProcessAfterInitialization");
		System.out.println("Bean Name : " + beanName);
		return beanObj;
	}
	
}
