package com.wipro.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("pointA")
@Scope("session")
public class Point {
	
	private int x ;
	private int y ;
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	
	

}
