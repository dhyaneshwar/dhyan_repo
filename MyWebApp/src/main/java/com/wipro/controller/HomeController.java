package com.wipro.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wipro.dao.StudentRepo;
import com.wipro.model.Student;

@Controller
public class HomeController {
	
	@Autowired
	private StudentRepo repo;
	
	@RequestMapping("/home")
	public /* @ResponseBody */ String home() {
		System.out.println("inside home controller");
		//this gonna create conflict and this needs to be fixed locally
		return "home";
		
	}
	
	@RequestMapping("/addStudent")
	public String addStudent(Student student) {
		try {
			repo.save(student);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		return "home";
	}

}
