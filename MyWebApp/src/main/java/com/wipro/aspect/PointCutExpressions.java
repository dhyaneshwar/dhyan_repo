package com.wipro.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
public abstract class PointCutExpressions {

	@Pointcut("execution(* draw*())")
	public void allDrawMethodsWithoutArgs() {
	};

	@Pointcut("execution(* draw*(..))")
	public void allDrawMethodsIrrespectiveArgs() {
	};

	@Pointcut("execution(* draw*(*))")
	public void allDrawMethodsWith1orMoreArgs() {
	};

	@Pointcut("within(com.wipro.service.impl.Circle)")
	public void allMethodsWithinCircle() {
	};

	/*
	 * @Pointcut("within(com.dhyanesh.wipro..*)") public void
	 * allMethodsWithinWipro() {}; // this is not working need to check why
	 */

	@Pointcut("args(String)")
	public void allMethodsWithSingleStringArg() {
	};

	@Pointcut("@annotation(com.wipro.aspect.LoggerAnnot)")
	public void allMethodsWithCustomAnnot() {
	};// method level only not class level

	@Pointcut("this(com.wipro.service.impl.Circle)") // in this case child class has to mentioned here
	public void toClassWhichDoesntInherit() {
	};// "this" to be used for classes which does not implement/extend any other
		// interfaces/classes

	@Pointcut("target(com.wipro.service.interfaces.Shape)") // in this case parent class/interface has to mentioned here
	public void toClassWhichInherit() {};// "target" to be used for classes which implement/extend any other interfaces/classes
	
	@Pointcut("@target(com.wipro.aspect.LoggerAnnot)")
	public void toClassWhichInheritAndHasAnnot() {};//this is not working need to check

	@Pointcut("@within(com.wipro.aspect.LoggerAnnot)")
	public void	toClassWithspecifiedAnnot() {};
	 

}
