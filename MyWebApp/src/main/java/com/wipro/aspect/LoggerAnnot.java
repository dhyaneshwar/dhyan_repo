package com.wipro.aspect;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;



@Retention(RUNTIME)
//@Target(TYPE)
public @interface LoggerAnnot {

}
