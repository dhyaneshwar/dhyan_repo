package com.wipro.dao;

import org.springframework.data.repository.CrudRepository;

import com.wipro.model.Student;


public interface StudentRepo extends CrudRepository<Student, Integer>{

}
