package com.wipro.configurations;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.wipro.dao.StudentRepo;

@SpringBootApplication
@ComponentScan(basePackages = "com.wipro")
@EnableJpaRepositories( basePackageClasses = StudentRepo.class )
public class MyWebAppApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext applnContext = SpringApplication.run(MyWebAppApplication.class, args);
		//locally added comment
		//Online added comments
	}
	
}
