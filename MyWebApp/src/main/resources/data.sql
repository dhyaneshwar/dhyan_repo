DROP TABLE IF EXISTS Student;
CREATE TABLE  Student(
  aid INT PRIMARY KEY,
  aname VARCHAR(250)
);

INSERT INTO Student (aid, aname) VALUES ('1','test');